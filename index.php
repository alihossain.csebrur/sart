
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Solution Art Ltd.</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/lightbox.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link id="css-preset" href="css/presets/preset1.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  
  <link href="css/custom.css" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="favicon.ico">
</head>
<!--/head-->
<body> 
 <!--HEADER
 ============================================================================-->
 <?php include'html/header.html';?>  

<section id="blog">
	<div class="container">
		<div class="row">
			<div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
				       
			</div>
		</div>
		
		<div class="blog-posts">		
			<h2 class="color text-center">Your Problems || Our Solutions || Solution Art</h2> 
			<div class="row">
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">			   
					<div class="entry-header">
						<h3 class="color">The Company</h3>             
					</div>
					<div class="entry-content">
						<p>
							'Solution Art' is a common platform of professionals to provide you any solution within their coverage, as per your need and will be delivered with utmost sincerity. We are one stop technology solution provider. But, we don't see ourselves simply as a technology solution provider company, rather as your trusted business consultant. We don't implement technology for technology's sake, but believe that technology should be implemented only when it is aligned with your business vision
						</p>
					</div>
				</div>
			  
			  
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">			   
					<div class="entry-header">
						<h3 class="color">Why Solution Art</h3>             
					</div>
					<div class="entry-content">
						<p>
							We acknowledge the strength of technology and also appreciate it is not technology but benefit that is useful to our clients. So, our solutions are balanced blend of technology, domain knowledge and value addition to user experience.				
						</p>
					</div>
				</div>
			  
			  
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">           
					<div class="entry-header">
						<h3 class="color">What we provide – Empowering Solutions</h3>             
					</div>
					<div class="entry-content">
						<p>
							We architect, design, supply, implement and maintain scalable solutions for our clients. Our solutions include wide variety of desktop or web based applications and mobility Solutions, which, enables our client to operate, manage and communicate efficiently & effectively.			  
						</p>
					</div>
				</div>
				<div class="clearfix"></div>
			  
			  
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">           
					<div class="entry-header">
						<h3 class="color">What we think - Technological Innovation</h3>             
					</div>
					<div class="entry-content">
						<p>
							We believe "Innovation" is continuous process and we focus on technological innovation to combine hardware, software and network effectively for our clients. Our innovation team explores the strength of new technologies to innovate benefits for better life.
						</p>
					</div>
				</div>
			  
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">           
					<div class="entry-header">
						<h3 class="color">What we Dream – The Journey</h3>             
					</div>
					<div class="entry-content">
						<p>
							We dream for a technology savvy society. Besides solution delivery we work on sharing knowledge and transferring skills. We maintain frank and rational dealings and explain what technology can do and what not. Our mission for better society is our journey not a destination.
						</p>
					</div>
				</div>			  
			  
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">           
					<div class="entry-header">
						<h3 class="color">Core Values</h3>             
					</div>
					<div class="entry-content">
						<p>
							Honest, Frank and Rational Dealings: Explain what technology can do and what not, Candid communication regarding planning and implementation, Establish justice to all stakeholders and Compassionate consideration.
							Leverage technology to facilitate empowerment: Facilitate resources, Enable and empower Client.
							Success is a journey, not a destination: Continuity, Service commitment, Skill up gradation, Seek for Human Qualities
						</p>
					</div>
				</div>		                     
			</div>                      
		</div>
	</div>
</section><!--#blog  Who We are-->


  
<section id="services">
	<div class="container">
		<div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="row">
				<div class="text-center col-sm-8 col-sm-offset-2">
					<h1 class="color">Our Services</h1>            
				</div>
			</div> 
		</div>
		<div class="text-center our-services">
			<div class="row">
				<div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
					<div class="service-icon">
						<i class="fa fa-desktop"></i>
					</div>
					<div class="service-info">
						<h3 class="color">PROJECT MANAGEMENT </h3>
							TURNKEY SOLUTION
						<p>
							We analyze and appreciate technology requirement for organizations. Be turn-key project or partial implementation or changes to IT infrastructure, we take up projects on behalf of out client and deliver the desired result. We share knowledge on general project management tools and technique and manage works by project management.
						</p>
					</div>
				</div>
				<div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
					<div class="service-icon">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="service-info">
						<h3 class="color">TAILORED DEVELOPMENT </h3>
							AS IT FITS
						<p>
							We take up projects for any customized solutions required by enterprises for desktop/web application or infrastructure. In working with our custom development experts, you will improve quality, reduce risk, and gain the capabilities and agility your business requires to seize new opportunities and respond to emerging business challenges.
						</p>
					</div>
				</div>
				<div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="550ms">
					<div class="service-icon">
					  <i class="fa fa-desktop"></i>
					</div>
					<div class="service-info">
						<h3 class="color">ENTERPRISE NETWORK</h3>
						IT INFRASTRUCTURE
						<p>
							We provide network planning and implementation service for your enterprise LAN, WAN, MAN and Internet. We setup customized voice and data network to enable efficient communication for your organization. Our technical experts can educate your human resource on how to implement and maintain a big network.
						</p>
					</div>
				</div>
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="650ms">
					<div class="service-icon">
						<i class="fa fa-desktop"></i>
					</div>
					<div class="service-info">
						<h3 class="color">TRAINING</h3> 
							HR DEVELOPMENT
						<p>
							Our aim is to inform people regarding IT usage by providing solutions and guidance on how to make good use out of it. We take up programs to build IT awareness so that people can make best use of IT infrastructure around them. We offer training programs, workshops, web based IT resources Center, etc. to create responsible IT consumer
						</p>
					</div>
				</div>
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="750ms">
					<div class="service-icon">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="service-info">
						<h3 class="color">MAINTENANCE</h3> 
							SUPPORT SERVICES
						<p>
							Managed IT services provide unlimited proactive support that not only keeps your network running effectively and efficiently, but it helps you reach your business goals. Our support team is organized to take care of your IT infrastructure maintenance by security assurance, troubleshooting, and data recovery and disaster management.
						</p>
					</div>
				</div>
				<div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="850ms">
					<div class="service-icon">
						<i class="fa fa-desktop"></i>
					</div>
					<div class="service-info">
						<h3 class="color">CONSULTING</h3>
							DATA HERE
						<p>
							Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here Data Here.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--END #services  What We Do-->
  
<section id="about-us" class="parallax">
	<div class="container">
		<div class="row">
			<h1 class="color text-center">How We Do</h1>
			<div class="col-sm-6">
				<div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">		  
					<h2>Requirement Analysis</h2>
					<p>
						Building great software is an investment of time and resources. So why not make that commitment knowing that your users and stakeholders share the same vision? Prototyping allows us to quickly build the initial version of your software so that real users can interact with it. Our eligible designers and engineers will help refine your ideas to perfection so that the final product will surpass everyone’s expectations.
					</p>            
				</div>
			</div>
			<div class="col-sm-6">
				<div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
					<h2>Design and development</h2>
					<p>
						People have become accustomed to the fluidity and polish of consumer applications on their phones, tablets, and other mobile devices. They expect the same polish in their enterprise line-of-business applications. Solution art’s design team provides that consumer application polish for your line-of-business web, mobile, desktop, and kiosk. You’re developing technology systems to operate core components of your business. Solution art will ensure that your software is delivered on time and on budget and that your solution is both practical and enduring for your business. We do more than develop software, we deliver lasting solutions.
					</p>            
				</div>
			</div>		
			<div class="clearfix"></div>
			
		
			<div class="col-sm-6">
				<div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
					<h2>Annual maintenance</h2>
					<p>
						In today’s economic world, business systems encompass a huge spectrum of activities to perform within the limited time and logistics. From the traditional ‘back office’ systems like accounts, payroll, etc. to direct operations, sales and customer services, etc. fall under the day to day operations of a modern business entity. Further the introduction of ERP applications that span across the complete business process workflow of an enterprise to the specialized CRM applications; IT processes and services face the continuous challenge of supporting, maintaining and upgrading the systems. To cater all the above challenges we provide various maintenance facilities.
					</p>			            
				</div>
			</div>			
			<div class="col-sm-6">
				<div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
					<h2 class="color">Implementation & Training</h2>
						<p>
							After getting your software ready as per your requirements we install them to your systems and provide necessary training to your users so that they can make the best use of our creation, for the best interest of your company.
						</p>            
				</div>
			</div>		
		</div><!--END .Row-->
	</div><!--END. Container-->
</section><!--END #about-us   How We Do-->
  
  
  <section id="team">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          
	   </div>
      </div>
	  
	  
      <div class="team-members">
	    <h2 class="text-center color">Our Skill</h2>
        <div class="row">
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div class="member-image">
                <img class="img-responsive" src="images/team/1.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Marian Dixon</h3>               
              </div>              
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="500ms">
              <div class="member-image">
                <img class="img-responsive" src="images/team/2.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Lawrence Lane</h3>                
              </div>             
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
              <div class="member-image">
                <img class="img-responsive" src="images/team/3.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Lois Clark</h3>
              </div>
             
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
              <div class="member-image">
                <img class="img-responsive" src="images/team/4.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Marian Dixon</h3>                
              </div>
              
            </div>
          </div>
        </div>
      </div>            
    </div>
  </section><!--/#team-->  
  
  

<section id="portfolio">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
          <h2 class="color">Our Portfolio</h2>          
        </div>
      </div> 
    </div>
    <div class="container">
		<div class="row">		  
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/dscsc.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>DSCSC</h3>
									<p>Defence Service Command &amp; staff College</p>
								</div>
								<div class="folio-overview">
									<a href="http://www.dscsc.mil.bd/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/dscsc.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/biam.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>BIAM</h3>
									<p>Bangladesh Instutite of Aministration &amp; Management</p>
								</div>
								<div class="folio-overview">
									<a href="http://www.biamfoundation.org/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/biam.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/sebl.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>SEBL</h3>
									<p>South East Bank Ltd.</p>
								</div>
								<div class="folio-overview">
									<a href="https://www.southeastbank.com.bd/home.php" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/sebl.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/inaniroyal.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>Inani Royal</h3>
									<p>Inani Royal Resorts</p>
								</div>
								<div class="folio-overview">
									<a href="http://inaniroyalresort.com/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/inaniroyal.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/padma.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>PMBP</h3>
									<p>Padma Multipurpose Bridge Project</p>
								</div>
								<div class="folio-overview">
									<a href="http://www.padmabridge.gov.bd/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/padma.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
       
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/led.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>LED</h3>
									<p>LED'S Limited</p>
								</div>
								<div class="folio-overview">
									<a href="http://ledsltd.com/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/led.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
			
		
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/kcsl.jpg" alt="">
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>KCSL</h3>
									<p>Korean Cosmatic Shop Limited</p>
								</div>
								<div class="folio-overview">
									<a href="https://koreancosmeticshopltd.com/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/kcsl.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-sm-3">
				<div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
					<div class="folio-image">
						<img class="img-responsive" src="images/client/edited/hex.jpg" alt="">				
					</div>
					<div class="overlay">
						<div class="overlay-content">
							<div class="overlay-text">
								<div class="folio-info">
									<h3>Hexagon</h3>
									<p>Hexagon Bangladesh</p>
								</div>
								<div class="folio-overview">
									<a href="http://www.hexagonbangladesh.com/" target="_blank"><i class="fa fa-link"></i></a>
									<span class="folio-expand"><a href="images/client/original/hex.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>
    </div><!--END .container-->	
</section><!--END #portfolio--> 


<div class="twitter-icon text-center">							
	<h1 class="color">On Going Projects</h1>
</div>
<section id="twitter" class="parallax">
	<div>
		<a class="twitter-left-control" href="#twitter-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
		<a class="twitter-right-control" href="#twitter-carousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
		<div class="container">
			<div class="row">					
				<div class="col-sm-8 col-sm-offset-2">						
					<div id="twitter-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>
							</div>
							<div class="item">
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>
								<div class="col-md-4">
									<h3>Sample Project-1</h3>
									<p>
										Data here data here data here data here. Data here data here data here data here.
									</p>
								</div>								
							</div>							
						</div>                        
					</div>                    
				</div>
			</div>
		</div>
	</div>
</section><!--END #twitter Our Gallery-->  
  
  
  

<section id="contact">
    <!--<div id="google-map" class="wow fadeIn" data-latitude="52.365629" data-longitude="4.871331" data-wow-duration="1000ms" data-wow-delay="400ms"></div>-->
	<div class="container-flued">		
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7298.959612461203!2d90.3657835!3d23.8370902!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c14a3366b005%3A0x901b07016468944c!2z4Kau4Ka_4Kaw4Kaq4KeB4KawIOCmoeCmvyzgppMs4KaP4KaH4KaaLOCmj-CmuCwg4Kai4Ka-4KaV4Ka-IDEyMTY!5e0!3m2!1sbn!2sbd!4v1464614034556" width="1400" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		
	</div>
	
	
	
	<div id="contact-us" class="parallax">
		<div class="container">
			<div class="row">
				<div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
					<h2>Contact Us</h2>            
				</div>
			</div>
			<div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
				<div class="row">
					<div class="col-sm-4">
						<h1>Office</h1>
						 <p>
							H #1333 (2nd Floor) <br>Avenue 02A, Road #13<br> 
							DOHS Mirpur, Dhaka 1216<br>Bangladesh
						 </p>
					</div>
					<div class="col-sm-4">
						<h1>Call Us</h1> 
						Phone:<a href="tel:+88 02 8080820"> +88 02 8080820</a><br>							
					</div>
					<div class="col-sm-4">
						<h1>Email Us</h1>
						<p>	
							Information: <a href="info@solutionart.net"> info@solutionart.net</a><br>				
							Sales: <a href="sales@solutionart.net"> sales@solutionart.net</a><br>
							Support: <a href="support@solutionart.net"> support@solutionart.net</a><br>		
						</p>
					</div>
				</div>
			</div>
		</div>
    </div>        
</section><!--/#contact-->
  
  <!--Footer
  =====================================================================-->
  <?php include'html/footer.html'?>
  

  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="js/jquery.inview.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/mousescroll.js"></script>
  <script type="text/javascript" src="js/smoothscroll.js"></script>
  <script type="text/javascript" src="js/jquery.countTo.js"></script>
  <script type="text/javascript" src="js/lightbox.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>

</body>
</html>